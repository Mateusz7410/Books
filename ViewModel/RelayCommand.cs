﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Babiaczyk.Books.UI2.ViewModels
{
    public class RelayCommand : ICommand
    {
        private readonly Action<object> _execute;
        private readonly Predicate<object> _canExecute;

        public RelayCommand(Action<object> action, Predicate<object> canExecute)
        {
            _execute = action;
            _canExecute = canExecute;
        }

        public RelayCommand(Action<object> action) : this(action, null)
        {

        }


        public event EventHandler CanExecuteChanged
        {
            remove { CommandManager.RequerySuggested -= value;  }
            add { CommandManager.RequerySuggested += value;  }
        }

        public bool CanExecute(object parameter)
        {
            if (_canExecute != null)
            {
                return _canExecute(parameter);
            }
            return true;
        }

        public void Execute(object parameter)
        {
            if (_execute != null)
            {
                _execute(parameter);
            }
        }
    }
}
