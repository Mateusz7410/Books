﻿using Babiaczyk.Books.CORE;
using Babiaczyk.Books.INTERFACES;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace Babiaczyk.Books.UI2.ViewModels
{
    public class BookViewModel : ViewModelBase
    {
        private IBook _book;

        public BookViewModel(IBook book)
        {
            _book = book;
        }

        [Required(ErrorMessage = "Author can not be empty")]
        public IAuthor Author
        {
            get => _book.Author;
            set
            {
                _book.Author = value;
                OnPropertyChanged(nameof(Author));
                Validate();
            }
        }

        public IBook GetBook()
        {
            return _book;
        }

        [Required(ErrorMessage = "Title can not be empty")]
        public string Title
        {
            get => _book.Title;
            set
            {
                _book.Title = value;
                OnPropertyChanged(nameof(Title));
                Validate();
            }
        }

        [Required(ErrorMessage = "Cover can not be empty")]
        public CoverType Cover
        {
            get => _book.Cover;
            set
            {
                _book.Cover = value;
                OnPropertyChanged(nameof(Cover));
                Validate();
            }
        }

        [Required(ErrorMessage = "Publisher can not be empty")]
        public string Publisher
        {
            get => _book.Publisher;
            set
            {
                _book.Publisher = value;
                OnPropertyChanged(nameof(Publisher));
                Validate();
            }
        }

        public void Validate()
        {
            var validationContext = new ValidationContext(this, null, null);
            var validationResults = new List<ValidationResult>();
            
            Validator.TryValidateObject(this, validationContext, validationResults, true);
            foreach (var err in _errors.ToList())
            {
                if (validationResults.All(res => res.MemberNames.All(m => m != err.Key)))
                    {
                        _errors.Remove(err.Key);
                        OnErrorChanged(err.Key);
                    }
            }

            var q = from e in validationResults
                    from m in e.MemberNames
                    group e by m into g
                    select g;
            
            foreach (var p in q)
            {
                var messages = p.Select(r => r.ErrorMessage).ToList();
                
                if (_errors.ContainsKey(p.Key))
                {
                    _errors.Remove(p.Key);
                }
                _errors.Add(p.Key, messages);
                OnErrorChanged(p.Key);
            }
        }

    }
}
