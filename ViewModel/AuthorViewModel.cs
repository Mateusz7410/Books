﻿using Babiaczyk.Books.CORE;
using Babiaczyk.Books.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Babiaczyk.Books.UI2.ViewModels
{
    public class AuthorViewModel : ViewModelBase
    {
        private IAuthor _author;

        public AuthorViewModel(IAuthor author)
        {
            _author = author;
        }

        public IAuthor GetAuthor()
        {
            return _author;
        }

        public override string ToString()
        {
            return Name;
        }

        [Required(ErrorMessage = "Name can not be empty")]
        public string Name
        {
            get => _author.Name;
            set
            {
                _author.Name = value;
                OnPropertyChanged(nameof(Name));
                Validate();
            }
        }

        [Required(ErrorMessage = "Country can not be empty")]
        public CountryName Country
        {
            get => _author.Country;
            set
            {
                _author.Country = value;
                OnPropertyChanged(nameof(Country));
                Validate();
            }
        }

        [Range(0, 2018, ErrorMessage = "You can not set year larger than actual (2018).")]
        [Required(ErrorMessage = "Year of birth can not be empty")]
        public int Year
        {
            get => _author.YearOfBirth;
            set
            {
                _author.YearOfBirth = value;
                OnPropertyChanged(nameof(Year));
                Validate();
            }
        }

        public void Validate()
        {
            var validationContext = new ValidationContext(this, null, null);
            var validationResults = new List<ValidationResult>();

            Validator.TryValidateObject(this, validationContext, validationResults, true);
            foreach (var err in _errors.ToList())
            {
                if (validationResults.All(res => res.MemberNames.All(m => m != err.Key)))
                {
                    _errors.Remove(err.Key);
                    OnErrorChanged(err.Key);
                }
            }

            var q = from e in validationResults
                    from m in e.MemberNames
                    group e by m into g
                    select g;

            foreach (var p in q)
            {
                var messages = p.Select(r => r.ErrorMessage).ToList();

                if (_errors.ContainsKey(p.Key))
                {
                    _errors.Remove(p.Key);
                }
                _errors.Add(p.Key, messages);
                OnErrorChanged(p.Key);
            }
        }
    }
}
