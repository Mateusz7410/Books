﻿using Babiaczyk.Books.BL;
using Babiaczyk.Books.CORE;
using Babiaczyk.Books.INTERFACES;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Babiaczyk.Books.UI2.ViewModels
{
    public class BookListViewModel : ViewModelBase
    {
        public ObservableCollection<BookViewModel> Books { get; set; } = new ObservableCollection<BookViewModel>();

        public List<CoverType> CoverTypes { get; } = new List<CoverType>();

        private ListCollectionView _view;

        public BookListViewModel()
        {
            GetAllBooks();
            GetAllCoverTypes();

            _editedBook = new BookViewModel(BL.BL.NewBook());
            _addedBook = new BookViewModel(BL.BL.NewBook());
            _addedBook.Validate();
            _selectedBook = new BookViewModel(BL.BL.NewBook());
            _view = (ListCollectionView)CollectionViewSource.GetDefaultView(Books);
            _addNewBookCommand = new RelayCommand(param => this.AddNewBook(), param => this.CanAddBook());
            _deleteBookCommand = new RelayCommand(param => this.DeleteBook());
            _editBookCommand = new RelayCommand(param => this.EditBook(), param => this.CanEditBook());
        }

        private void GetAllBooks()
        {
            foreach (var book in BL.BL.GetBooks())
            {
                Books.Add(new BookViewModel(book));
            }
        }

        private void GetAllCoverTypes()
        {
            foreach (var cover in Enum.GetValues(typeof(CoverType)))
            {
                CoverTypes.Add((CoverType)cover);
            }
        }

        private BookViewModel _addedBook;
        public BookViewModel AddedBook
        {
            get => _addedBook;
            set
            {
                _addedBook = value;
                OnPropertyChanged(nameof(AddedBook));
            }
        }

        private BookViewModel _editedBook;

        private bool CanEditBook()
        {
            return !EditedBook.HasErrors;
        }
        public BookViewModel EditedBook
        {
            get => _editedBook;
            set
            {
                _editedBook = value;
                OnPropertyChanged(nameof(EditedBook));
            }
        }

        private BookViewModel _selectedBook;
        public BookViewModel SelectedBook
        {
            get => _selectedBook;
            set
            {
                if (value != null)
                {
                    _editedBook.Title = value.Title;
                    _editedBook.Publisher = value.Publisher;
                    _editedBook.Cover = value.Cover;
                    _editedBook.Author = value.Author;
                    _selectedBook = value;
                    OnPropertyChanged(nameof(SelectedBook));
                }
            }
        }


        private void AddNewBook()
        {
            if (BL.BL.AddBook(AddedBook.GetBook()))
            {
                Books.Add(AddedBook);
                AddedBook = new BookViewModel(BL.BL.NewBook());
                MessageBox.Show("Book was add", "Book was add");
                AddedBook.Validate();
            }
            else
            {
                MessageBox.Show("Book can not be added, already exists", "Book Exists");
            }
        }

        private RelayCommand _addNewBookCommand;

        private bool CanAddBook()
        {
            return !AddedBook.HasErrors;
        }

        public RelayCommand AddNewBookCommand
        {
            get => _addNewBookCommand;
        }

        private RelayCommand _editBookCommand;
        public RelayCommand EditBookCommand
        {
            get => _editBookCommand;
        }

        private void EditBook()
        {
            if(EditedBook.Title != null && EditedBook.Author != null)
            {
                MessageBoxResult result = MessageBox.Show("Are you sure about editing this book?", "Confirm", MessageBoxButton.YesNo);
                if(result == MessageBoxResult.Yes)
                {
                    BL.BL.UpdateBook(EditedBook.GetBook());
                }
            }
        }

        private RelayCommand _deleteBookCommand;

        public RelayCommand DeleteBookCommand
        {
            get => _deleteBookCommand;
        }

        private void DeleteBook()
        {
            if (SelectedBook.Publisher != null && SelectedBook.Title != null)
            {
                string messageText = $"Do you want to delete book: {SelectedBook.Title}?";
                MessageBoxResult result = MessageBox.Show(messageText, "Confirm", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    if (BL.BL.DeleteBook(SelectedBook.GetBook()))
                    {
                        Books.Remove(SelectedBook);
                    }
                    SelectedBook = new BookViewModel(BL.BL.NewBook());
                }
            }
        }
    }
}
