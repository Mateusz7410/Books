﻿using Babiaczyk.Books.INTERFACES;
using System;
using Babiaczyk.Books.CORE;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Babiaczyk.Books.UI2.ViewModels
{
    public class AuthorListViewModel : ViewModelBase
    {
        public ObservableCollection<AuthorViewModel> Authors { get; set; } = new ObservableCollection<AuthorViewModel>();

        public List<CountryName> CountryNames { get; } = new List<CountryName>();
        public ObservableCollection<IAuthor> AuthorSelectList { get; set; } = new ObservableCollection<IAuthor>();

        private ListCollectionView _view;

        public AuthorListViewModel()
        {
            GetAllAuthors();
            GetAllCountryNames();

            _selectedAuthor = new AuthorViewModel(BL.BL.NewAuthor());
            _editedAuthor = new AuthorViewModel(BL.BL.NewAuthor());
            _addedAuthor = new AuthorViewModel(BL.BL.NewAuthor());
            _addedAuthor.Validate();
            _view = (ListCollectionView)CollectionViewSource.GetDefaultView(Authors);
            _addNewAuthorCommand = new RelayCommand(param => this.AddNewAuthor());
            _deleteAuthorCommand = new RelayCommand(param => this.DeleteAuthor());
            _editAuthorCommand = new RelayCommand(param => this.EditAuthor(), param => this.CanEditAuthor());
        }

        private void GetAllAuthors()
        {
            foreach (var author in BL.BL.GetAuthors())
            {
                Authors.Add(new AuthorViewModel(author));
                AuthorSelectList.Add(author);
            }
        }

        private void GetAllCountryNames()
        {
            foreach (var cover in Enum.GetValues(typeof(CountryName)))
            {
                CountryNames.Add((CountryName)cover);
            }
        }

        private AuthorViewModel _addedAuthor;
        public AuthorViewModel AddedAuthor
        {
            get => _addedAuthor;
            set
            {
                _addedAuthor = value;
                OnPropertyChanged(nameof(AddedAuthor));
            }
        }

        private AuthorViewModel _editedAuthor;

        private bool CanEditAuthor()
        {
            return !EditedAuthor.HasErrors;
        }
        public AuthorViewModel EditedAuthor
        {
            get => _editedAuthor;
            set
            {
                _editedAuthor = value;
                OnPropertyChanged(nameof(EditedAuthor));
            }
        }

        private AuthorViewModel _selectedAuthor;
        public AuthorViewModel SelectedAuthor
        {
            get => _selectedAuthor;
            set
            {
                if(value != null)
                {
                    _editedAuthor.Name = value.Name;
                    _editedAuthor.Year = value.Year;
                    _editedAuthor.Country = value.Country;
                    _selectedAuthor = value;
                    OnPropertyChanged(nameof(SelectedAuthor));
                }
                
            }
        }

        private void AddNewAuthor()
        {
            if (BL.BL.AddAuthor(AddedAuthor.GetAuthor()))
            {
                AuthorSelectList.Add(AddedAuthor.GetAuthor());
                Authors.Add(AddedAuthor);
                AddedAuthor = new AuthorViewModel(BL.BL.NewAuthor());
                MessageBox.Show("Author was add", "Author was add");
                AddedAuthor.Validate();
            }
            else
            {
                MessageBox.Show("Author was not add, already exists in database.", "Author Exists");
            }
        }

        private bool CanAddAuthor()
        {
            return !AddedAuthor.HasErrors;
        }

        private RelayCommand _addNewAuthorCommand;

        public RelayCommand AddNewAuthorCommand
        {
            get => _addNewAuthorCommand;
        }

        private RelayCommand _editAuthorCommand;

        public RelayCommand EditAuthorCommand
        {
            get => _editAuthorCommand;
        }

        private void EditAuthor()
        {
            if(EditedAuthor.Name != null)
            {
                MessageBoxResult res = MessageBox.Show("Are you sure about editing this Author?", "Confirm", MessageBoxButton.YesNo);
                if(res == MessageBoxResult.Yes)
                {
                    BL.BL.UpdateAuthor(EditedAuthor.GetAuthor());
                }
            }
        }

        private void DeleteAuthor()
        {
            if (SelectedAuthor.Name != null)
            {
                MessageBoxResult res = MessageBox.Show($"Are you sure about deleting {SelectedAuthor.Name}?", "Confirm", MessageBoxButton.YesNo);
                if (res == MessageBoxResult.Yes)
                {
                    if (BL.BL.DeleteAuthor(SelectedAuthor.GetAuthor()))
                    {
                        AuthorSelectList.Remove(SelectedAuthor.GetAuthor());
                        Authors.Remove(SelectedAuthor);
                        SelectedAuthor = new AuthorViewModel(BL.BL.NewAuthor());
                    }
                }
            }
        }

        private RelayCommand _deleteAuthorCommand;
        public RelayCommand DeleteAuthorCommand
        {
            get => _deleteAuthorCommand;
        }
    }
}
