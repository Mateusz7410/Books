﻿using Babiaczyk.Books.INTERFACES;
using System;

namespace Babiaczyk.Books.UI
{
    public class UI
    {
        protected static void Main(string[] args)
        {
            var dbType = Properties.Settings.Default.DataBase;
            IBL ibl = new BL.BL(dbType);
            Console.WriteLine("All Publishers:");
            foreach(var publisher in ibl.GetPublishers())
            {
                Console.WriteLine(publisher.ToString());
            }
            Console.WriteLine();
            Console.WriteLine("All Books:");
            foreach (var book in ibl.GetBooks())
            {
                Console.WriteLine(book.ToString());
            }
            Console.WriteLine();
            Console.WriteLine("All Titles:");
            foreach (var title in ibl.GetTitles())
            {
                Console.WriteLine(title.ToString());
            }
            Console.WriteLine("All Authors:");
            foreach (var author in ibl.GetAuthors())
            {
                Console.WriteLine(author.ToString());
            }
            Console.ReadKey();
        }
    }
}
