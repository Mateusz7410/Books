﻿using Babiaczyk.Books.INTERFACES;
using Babiaczyk.Books.DAO;
using System.Collections.Generic;
using Babiaczyk.Books.CORE;
using System.Reflection;
using System;
using System.IO;

namespace Babiaczyk.Books.BL
{
    public class BL 
    {
        private static IDAO _dao;

        public BL(string dbType)
        {
            try
            {
                var dllFile = new FileInfo(@"..\..\..\BL\bin\Debug\" + dbType + ".dll");
                Assembly db = Assembly.UnsafeLoadFrom(dllFile.FullName);
                Type daoType = null;
                foreach (var t in db.GetTypes())
                {
                    foreach (var i in t.GetInterfaces())
                    {
                        if (i.Name == "IDAO")
                            daoType = t;
                    }
                }
                ConstructorInfo constructorInfo = daoType.GetConstructor(new Type[] { });
                _dao = (IDAO)constructorInfo.Invoke(new Type[] { });
            }
            catch (Exception)
            {
                Console.WriteLine("Error with library path");
            }


        }

        public static bool UpdateBook(IBook book)
        {
            return _dao.UpdateBook(book);
        }

        public static bool UpdateAuthor(IAuthor author)
        {
            return _dao.UpdateAuthor(author);
        }

        public static IBook NewBook()
        {
            return _dao.NewBook();
        }

        public static IAuthor NewAuthor()
        {
            return _dao.NewAuthor();
        }

        public static List<IBook> GetBooks()
        {
            return _dao.GetBooksData();
        }

        public static List<IAuthor> GetAuthors()
        {
            return _dao.GetAuthorsData();
        }


        public static bool AddBook(IBook book)
        {
            return _dao.AddBook(book);
        }

        public static bool AddAuthor(IAuthor author)
        {
            return _dao.AddAuthor(author);
        }

        public static bool DeleteBook(IBook book)
        {
            return _dao.DeleteBook(book);
        }

        public static bool DeleteAuthor(IAuthor author)
        {
            return _dao.DeleteAuthor(author);
        }

        
    }
}
