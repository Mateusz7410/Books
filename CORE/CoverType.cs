﻿namespace Babiaczyk.Books.CORE
{
    public enum CoverType
    {
        Booklet,
        Paper,
        Cloth,
        Plastics,
        Cardboard
    }
}
