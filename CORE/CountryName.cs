﻿namespace Babiaczyk.Books.CORE
{
    public enum CountryName
    {
        Poland,
        Germany,
        UnitedStates,
        RPA,
        CzechRepublic,
        Ukraine,
        GreatBritain,
        Canada,
        China,
        Israel
    }
}
