﻿using System.Collections.Generic;
using Babiaczyk.Books.INTERFACES;
using Babiaczyk.Books.CORE;
using System.Linq;

namespace Babiaczyk.Books.DAO2
{
    public class DAOMock : IDAO
    {
        private List<IBook> _books;
        private List<IAuthor> _authors;

        public DAOMock()
        {
            _authors = new List<IAuthor>();
            _authors.Add(new Author(CountryName.UnitedStates, "Scott Lynch", 1978));
            _authors.Add(new Author(CountryName.UnitedStates, "Jim Butcher", 1971));
            _authors.Add(new Author(CountryName.UnitedStates, "Shanon Hale", 1974));
            _authors.Add(new Author(CountryName.GreatBritain, "Charlie Jane Anders", 1983));
            _authors.Add(new Author(CountryName.Germany, "Laini Taylor", 1971));
            _authors.Add(new Author(CountryName.CzechRepublic, "Emily Skrutskie", 1989));
            _authors.Add(new Author(CountryName.China, "Naomi Novik", 1973));
            _authors.Add(new Author(CountryName.UnitedStates, "Rachel Hartman", 1972));
            _authors.Add(new Author(CountryName.RPA, "Scott Westerfeld", 1963));
            _authors.Add(new Author(CountryName.Israel, "Leigh Bardugo", 1975));

            _books = new List<IBook>();
            _books.Add(new Book("Shogakukan", "The Lies of Locke Lamora", _authors.Find(x => x.Name == "Scott Lynch"), CoverType.Booklet));
            _books.Add(new Book("Shogakukan", "Storm Front", _authors.Find(x => x.Name == "Jim Butcher"), CoverType.Cardboard));
            _books.Add(new Book("Simon & Schuster", "Princess Academy", _authors.Find(x => x.Name == "Shanon Hale"), CoverType.Cloth));
            _books.Add(new Book("Simon & Schuster", "All the Birds in the Sky", _authors.Find(x => x.Name == "Charlie Jane Anders"), CoverType.Paper));
            _books.Add(new Book("Grupo Santillana", "Doughter of Smoke", _authors.Find(x => x.Name == "Laini Taylor"), CoverType.Plastics));
            _books.Add(new Book("Grupo Santillana", "The Abyss Surrounds Us", _authors.Find(x => x.Name == "Emily Skrutskie"), CoverType.Booklet));
            _books.Add(new Book("Klett", "Uprooted", _authors.Find(x => x.Name == "Naomi Novik"), CoverType.Cardboard));
            _books.Add(new Book("Klett", "Seraphina", _authors.Find(x => x.Name == "Rachel Hartman"), CoverType.Cloth));
            _books.Add(new Book("Mondadori", "Leviathan", _authors.Find(x => x.Name == "Scott Westerfeld"), CoverType.Paper));
            _books.Add(new Book("Mondadori", "Crooked Kingdom", _authors.Find(x => x.Name == "Leigh Bardugo"), CoverType.Plastics));

            
        }

        public List<IBook> GetBooksData()
        {
            return _books;
        }

        public List<IAuthor> GetAuthorsData()
        {
            return _authors;
        }



        public bool AddBook(IBook book)
        {
            if (_books.Contains(book))
            {
                return false;
            }
            else
            {
                _books.Add(book);
                return true;
            }
        }

        public bool AddBook(string publisher, string title, IAuthor author, CoverType cover)
        {
            var book = new Book(publisher, title, author, cover);
            if (_books.Contains(book))
            {
                return false;
            }
            else
            {
                _books.Add(book);
                return true;
            }
        }

        public bool AddAuthor(IAuthor author)
        {
            if (_authors.Contains(author))
            {
                return false;
            }
            else
            {
                _authors.Add(author);
                return true;
            }
        }

        public bool UpdateBook(IBook book)
        {
            if (_books.Contains(book))
            {
                _books.Where(x => x.Equals(book)).ToList().ForEach(x => { x.Cover = book.Cover; x.Publisher = book.Publisher; });
                return true;
            }
            return false;
        }

        public bool UpdateAuthor(IAuthor author)
        {
            if (_authors.Contains(author))
            {
                _authors.Where(x => x.Equals(author)).ToList().ForEach(x => { x.Country = author.Country; });
                return true;
            }
            return false;
        }

        public bool AddAuthor(CountryName countryName, string name, int year)
        {
            var author = new Author(countryName, name, year);
            if (_authors.Contains(author))
            {
                return false;
            }
            else
            {
                _authors.Add(author);
                return true;
            }
        }

        public bool DeleteBook(IBook book)
        {
            if (_books.Contains(book))
            {
                _books.Remove(book);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteBook(string publisher, IAuthor author, string title)
        {
            var book = new Book(publisher, title, author, 0);
            if (_books.Contains(book))
            {
                _books.Remove(book);
                return true;
            }
            else
            {
                return false;
            }
        }


        public bool DeleteAuthor(IAuthor author)
        {
            if (_authors.Contains(author))
            {
                _authors.Remove(author);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteAuthor(string name)
        {
            var author = new Author(0, name, 0);
            if (_authors.Contains(author))
            {
                _authors.Remove(author);
                return true;
            }
            else
            {
                return false;
            }
        }

        private int CountAuthorsBook(IAuthor author)
        {
            return _books.Where(x => x.Author.Equals(author)).Count();
        }

        public bool UpdateBook(string newPublisher, IAuthor author, string title, CoverType newCover)
        {
            var book = new Book("", title, author, 0);
            if (_books.Contains(book))
            {
                _books.Where(x => x.Equals(book)).ToList().ForEach(x => { x.Cover = newCover; x.Publisher = newPublisher; });
                return true;
            }
            return false;
        }

        public bool UpdateAuthor(string name, CountryName newCountry, int year)
        {
            var author = new Author(0, name, year);
            if (_authors.Contains(author))
            {
                _authors.Where(x => x.Equals(author)).ToList().ForEach(x => { x.Country = newCountry; });
                return true;
            }
            return false;
        }

        public IBook NewBook()
        {
            return new Book();
        }

        public IAuthor NewAuthor()
        {
            return new Author();
        }
    }
}
