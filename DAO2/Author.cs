﻿using Babiaczyk.Books.INTERFACES;
using Babiaczyk.Books.CORE;

namespace Babiaczyk.Books.DAO2
{
    public class Author : IAuthor
    {
        private CountryName _country;
        private string _name;
        private int _yearOfBirth;

        public Author()
        {

        }

        public Author(CountryName country, string name, int yearOfBirth)
        {
            Country = country;
            Name = name;
            YearOfBirth = yearOfBirth;
        }

        public CountryName Country
        {
            get => _country;
            set => _country = value;
        }

        public string Name
        {
            get => _name;
            set => _name = value;
        }

        public int YearOfBirth
        {
            get => _yearOfBirth;
            set => _yearOfBirth = value;
        }

        public bool Equals(IAuthor other)
        {
            if (Name == other.Name && YearOfBirth == other.YearOfBirth)
            {
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
