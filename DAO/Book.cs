﻿using Babiaczyk.Books.INTERFACES;
using Babiaczyk.Books.CORE;

namespace Babiaczyk.Books.DAO
{
    public class Book : IBook
    {
        private CoverType _cover;
        private string _publisher;
        private string _title;
        private IAuthor _author;

        public Book()
        {

        }

        public Book(string publisher, string title, IAuthor author, CoverType cover)
        {
            Publisher = publisher;
            Title = title;
            Author = author;
            Cover = cover;
        }

        public CoverType Cover
        {
            get => _cover;
            set => _cover = value;
        }

        public string Publisher
        {
            get => _publisher;
            set => _publisher = value;
        }

        public string Title
        {
            get => _title;
            set => _title = value;
        }

        public IAuthor Author
        {
            get => _author;
            set => _author = value;
        }

        public bool Equals(IBook other)
        {
            if(Author == other.Author && Title == other.Title)
            {
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            return "Title: " + Title + " | Author: " + Author + " | Publisher: " + Publisher + " | Cover type: " + Cover;
        }
    }
}
