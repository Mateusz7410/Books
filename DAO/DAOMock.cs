﻿using System.Collections.Generic;
using Babiaczyk.Books.INTERFACES;
using Babiaczyk.Books.CORE;
using System.Linq;

namespace Babiaczyk.Books.DAO
{
    public class DAOMock : IDAO
    {
        private List<IBook> _books;
        private List<IAuthor> _authors;

        public DAOMock()
        {

            _authors = new List<IAuthor>();
            _authors.Add(new Author(CountryName.UnitedStates, "Patrick Rothfuss", 1973));
            _authors.Add(new Author(CountryName.UnitedStates, "Lev Grossman", 1969));
            _authors.Add(new Author(CountryName.GreatBritain, "Terry Pratchett", 1948));
            _authors.Add(new Author(CountryName.RPA, "J.R.R. Tolkien", 1892));
            _authors.Add(new Author(CountryName.GreatBritain, "Joe Abercrombie", 1974));
            _authors.Add(new Author(CountryName.UnitedStates, "George R.R. Martin", 1948));

            _books = new List<IBook>();
            _books.Add(new Book("Pearson", "The Name of The Wind", _authors.Find(x => x.Name == "Patrick Rothfuss") , CoverType.Booklet));
            _books.Add(new Book("Pearson", "A Game of Thrones", _authors.Find(x => x.Name == "George R.R. Martin"), CoverType.Cardboard));
            _books.Add(new Book("China South Publishing Company", "A Clash of Kings", _authors.Find(x => x.Name == "George R.R. Martin"), CoverType.Cloth));
            _books.Add(new Book("China South Publishing Company", "A Storm of Swords", _authors.Find(x => x.Name == "George R.R. Martin"), CoverType.Paper));
            _books.Add(new Book("Grupo Planeta", "A Dance with Dragons", _authors.Find(x => x.Name == "George R.R. Martin"), CoverType.Plastics));
            _books.Add(new Book("Grupo Planeta", "The Wise Man's Fear", _authors.Find(x => x.Name == "Patrick Rothfuss"), CoverType.Booklet));
            _books.Add(new Book("Hachette Livre", "Doors of Stone", _authors.Find(x => x.Name == "Patrick Rothfuss"), CoverType.Cardboard));
            _books.Add(new Book("Hachette Livre", "The Blade Itself", _authors.Find(x => x.Name == "Joe Abercrombie"), CoverType.Cloth));
            _books.Add(new Book("Phoenix Publishing And Media Company", "Before They Are Hanged", _authors.Find(x => x.Name == "Joe Abercrombie"), CoverType.Paper));
            _books.Add(new Book("Holtzbrinck", "Last Arguments of Kings", _authors.Find(x => x.Name == "Joe Abercrombie"), CoverType.Plastics));
            _books.Add(new Book("Holtzbrinck", "Best Served Cold", _authors.Find(x => x.Name == "Joe Abercrombie"), CoverType.Booklet));
            _books.Add(new Book("McGraw Hill Education", "The Heroes", _authors.Find(x => x.Name == "Joe Abercrombie"), CoverType.Cardboard));
            _books.Add(new Book("McGraw Hill Education", "Red Country", _authors.Find(x => x.Name == "Joe Abercrombie"), CoverType.Cloth));
            _books.Add(new Book("Penguin Random House", "The Fellowship of the Ring", _authors.Find(x => x.Name == "J.R.R. Tolkien"), CoverType.Paper));
            _books.Add(new Book("Penguin Random House", "The Two Towers", _authors.Find(x => x.Name == "J.R.R. Tolkien"), CoverType.Plastics));
            _books.Add(new Book("Phoenix Publishing And Media Company", "The King Returns", _authors.Find(x => x.Name == "J.R.R. Tolkien"), CoverType.Booklet));
            _books.Add(new Book("RELX Group", "The Magicians", _authors.Find(x => x.Name == "Lev Grossman"), CoverType.Cardboard));
            _books.Add(new Book("RELX Group", "The Magician King", _authors.Find(x => x.Name == "Lev Grossman"), CoverType.Cloth));
            _books.Add(new Book("Scholastic", "The Magician's Land", _authors.Find(x => x.Name == "Lev Grossman"), CoverType.Paper));
            _books.Add(new Book("Scholastic", "The Colour of Magic", _authors.Find(x => x.Name == "Terry Pratchett"), CoverType.Plastics));
            _books.Add(new Book("Thomson Reuters", "Wyrd Sisters", _authors.Find(x => x.Name == "Terry Pratchett"), CoverType.Booklet));
            _books.Add(new Book("Thomson Reuters", "Eric", _authors.Find(x => x.Name == "Terry Pratchett"), CoverType.Cardboard));
            _books.Add(new Book("Wolters Kluwer", "Small Gods", _authors.Find(x => x.Name == "Terry Pratchett"), CoverType.Cloth));
            _books.Add(new Book("Wolters Kluwer", "Soul Music", _authors.Find(x => x.Name == "Terry Pratchett"), CoverType.Paper));

            
        }

        public List<IBook> GetBooksData()
        {
            return _books;
        }

        public List<IAuthor> GetAuthorsData()
        {
            return _authors;
        }

        public bool AddBook(IBook book)
        {
            if (_books.Contains(book))
            {
                return false;
            }
            else
            {
                _books.Add(book);
                return true;
            }
        }

        public bool AddBook(string publisher, string title, IAuthor author, CoverType cover)
        {
            var book = new Book(publisher, title, author, cover);
            if (_books.Contains(book))
            {
                return false;
            }
            else
            {
                _books.Add(book);
                return true;
            }
        }

        public bool AddAuthor(IAuthor author)
        {
            if (_authors.Contains(author))
            {
                return false;
            }
            else
            {
                _authors.Add(author);
                return true;
            }
        }

        public bool UpdateBook(IBook book)
        {
            if (_books.Contains(book))
            {
                _books.Where(x => x.Equals(book)).ToList().ForEach(x => { x.Cover = book.Cover; x.Publisher = book.Publisher; });
                return true;
            }
            return false;
        }

        public bool UpdateAuthor(IAuthor author)
        {
            if (_authors.Contains(author))
            {
                _authors.Where(x => x.Equals(author)).ToList().ForEach(x => { x.Country = author.Country; });
                return true;
            }
            return false;
        }

        public bool AddAuthor(CountryName countryName, string name, int year)
        {
            var author = new Author(countryName, name, year);
            if (_authors.Contains(author))
            {
                return false;
            }
            else
            {
                _authors.Add(author);
                return true;
            }
        }

        public bool DeleteBook(IBook book)
        {
            if (_books.Contains(book))
            {
                _books.Remove(book);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteBook(string publisher, IAuthor author, string title)
        {
            var book = new Book(publisher, title, author, 0);
            if (_books.Contains(book))
            {
                _books.Remove(book);
                return true;
            }
            else
            {
                return false;
            }
        }


        public bool DeleteAuthor(IAuthor author)
        {
            if (_authors.Contains(author))
            {
                _authors.Remove(author);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteAuthor(string name)
        {
            var author = new Author(0, name, 0);
            if (_authors.Contains(author))
            {
                _authors.Remove(author);
                return true;
            }
            else
            {
                return false;
            }
        }

        private int CountAuthorsBook(IAuthor author)
        {
            return _books.Where(x => x.Author.Equals(author)).Count();
        }

        public bool UpdateBook(string newPublisher, IAuthor author, string title, CoverType newCover)
        {
            var book = new Book("", title, author, 0);
            if (_books.Contains(book))
            {
                _books.Where(x => x.Equals(book)).ToList().ForEach(x => { x.Cover = newCover; x.Publisher = newPublisher; });
                return true;
            }
            return false;
        }

        public bool UpdateAuthor(string name, CountryName newCountry, int year)
        {
            var author = new Author(0, name, year);
            if (_authors.Contains(author))
            {
                _authors.Where(x => x.Equals(author)).ToList().ForEach(x => { x.Country = newCountry; });
                return true;
            }
            return false;
        }

        public IBook NewBook()
        {
            return new Book();
        }

        public IAuthor NewAuthor()
        {
            return new Author();
        }
    }
}
