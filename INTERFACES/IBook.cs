﻿using Babiaczyk.Books.CORE;
using System;

namespace Babiaczyk.Books.INTERFACES
{
    public interface IBook : IEquatable<IBook>
    {
        CoverType Cover
        {
            set;
            get;
        }
        string Publisher
        {
            set;
            get;
        }
        string Title
        {
            set;
            get;
        }

        IAuthor Author
        {
            set;
            get;
        }
    }
}
