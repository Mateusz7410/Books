﻿using Babiaczyk.Books.CORE;
using System;

namespace Babiaczyk.Books.INTERFACES
{
    public interface IAuthor : IEquatable<IAuthor>
    {
        string Name
        {
            set;
            get;
        }

        int YearOfBirth
        {
            set;
            get;
        }

        CountryName Country
        {
            set;
            get;
        }
    }
}
