﻿using System.Collections.Generic;
using Babiaczyk.Books.CORE;

namespace Babiaczyk.Books.INTERFACES
{
    public interface IBL
    {
        List<IBook> GetBooks();
        List<IAuthor> GetAuthors();
    }
}
