﻿using System.Collections.Generic;
using Babiaczyk.Books.CORE;

namespace Babiaczyk.Books.INTERFACES
{
    public interface IDAO
    {
        List<IBook> GetBooksData();
        List<IAuthor> GetAuthorsData();

        IBook NewBook();
        IAuthor NewAuthor();

        bool AddBook(IBook book);
        bool AddBook(string publisher, string title, IAuthor author, CoverType cover);
        bool AddAuthor(IAuthor author);
        bool AddAuthor(CountryName countryName, string name, int year);


        bool DeleteBook(IBook book);
        bool DeleteBook(string publisher, IAuthor author, string title);
        bool DeleteAuthor(IAuthor author);
        bool DeleteAuthor(string name);

        bool UpdateBook(string newPublisher, IAuthor author, string title, CoverType newCover);
        bool UpdateBook(IBook book);
        bool UpdateAuthor(string name, CountryName newCountry, int year);
        bool UpdateAuthor(IAuthor author);
    }
}
